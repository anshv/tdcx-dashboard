import React, {useState, useEffect} from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Login from './components/Login/Login';
import Header from './components/Header/Header';
import Dashboard from './components/Dashboard/Dashboard';
import { getToken, removeUserSession, setUserSession } from './Utils/Auth';

function App() { 
  const [authLoading, setAuthLoading] = useState(true);

  useEffect(() => {
    // const token = getToken();
    // if (!token) {
    //   return;
    // }
  })
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/dashboard">
          <Header />
          <Dashboard />
          </Route>
          <Route path="/">
            <Login />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
