import React from 'react'
import {
    Chart,
    PieSeries,
    Title,
  } from '@devexpress/dx-react-chart-material-ui';
import Paper from '@material-ui/core/Paper';
import './PieChart.css';

function PieChart() {
    const chartData = [
        { country: 'Russia', area: 12 },
        { country: 'Canada', area: 5 },
      ];

    return (
        <Paper>
        <Chart
          data={chartData}
        >
          <PieSeries
            valueField="area"
            argumentField="country"
          />
        </Chart>
      </Paper>
    )
}

export default PieChart
