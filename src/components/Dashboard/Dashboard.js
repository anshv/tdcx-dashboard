import React, { Fragment, useContext, useState, useEffect } from 'react'
import TaskDetail from '../TaskDetail/TaskDetail';
import './Dashboard.css';
import DashboardService from "./../../services/DashboardService";
import NewTask from './../NewTask/NewTask';
import Dialog from '@material-ui/core/Dialog';
import { TaskContext } from './../../context/TaskContext';
import PieChart from '../Chart/PieChart'

export default function Dashboard() {
    const [state, dispatch] = useContext(TaskContext);
    const [searchTerm, setSearchTerm] = useState('');
    const [openTaskModal, setTaskModal] = useState(false);
    const setTaskModalOpen = () => {
        setTaskModal(true);
    }
    const setTaskModalClose = () => {
        setTaskModal(false);
    }
    const searchTasks = (searchTerm) => {
        if (searchTerm !== '') {
            dispatch({
                type: 'SEARCH_TASK',
                payload: searchTerm
            });
        } else {
            loadTasks();
        }
    }
    const loadTasks = () => {
        DashboardService.getDashboardData().then(response => {
            dispatch({
                type: 'FETCH_TASKS',
                payload: response.data || response.data.data
            });
        })
    }
    useEffect(() => {
        const fetchTasks = async () => {
            loadTasks();
        };
        fetchTasks();
    }, [dispatch]);

    return (
        state.totalTasks > 0 ? (
            <Fragment>
                <div className="dashboard">
                    <div className="dashboard__container">
                        <h3>Tasks Completed</h3>
                        <div className="dashboard__counts"><span className="first">{state.tasksCompleted}</span>/ {state.totalTasks}</div>
                    </div>
                    <div className="dashboard__container">
                        <h3>Latest Created Tasks</h3>
                        <ul>
                            {state.tasks?.length
                                ? state.tasks.map((task) => (
                                    <li className={task.completed ? 'dashboard__checked' : ''}>{task.name}</li>
                                )) : ''}
                        </ul>
                    </div>
                    <div className="dashboard__container">
                        <PieChart />
                    </div>
                </div>
                <TaskDetail items={state.tasks} searchTaskEvent={searchTasks} />
            </Fragment>
        ) :
            <>
                <div className="noTask">
                    <div className="noTask__container">
                        <h3>You have no task.</h3>
                        <input type="button" className="noTask__btn" value="+ New Task" onClick={setTaskModalOpen} />
                    </div>
                </div>
                <Dialog open={openTaskModal} onClose={setTaskModalClose} aria-labelledby="form-dialog-title">
                    <NewTask taskModalClose={() => setTaskModalClose()} />
                </Dialog>
            </>
    )
}
