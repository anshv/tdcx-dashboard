import React from 'react'
import {BASE_URL} from '../../Utils/Constant';
import { Link } from "react-router-dom";
import './Header.css';
import {getUser} from '../../Utils/Auth';
export default function Header() {
    const profileImage = BASE_URL + '/images/profile.jpg';
    const userName = getUser().toUpperCase();
    return (
        <div className="header">
            <div className="header__left">
                <img className="header__photo" src={profileImage} alt="" />
                <p className="header__user">{userName}</p>
            </div>
            <div className="header__right">
                <Link to="/logout">Logout</Link>
            </div>
        </div>
    )
}
