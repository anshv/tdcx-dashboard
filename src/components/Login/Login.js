import React , { useState } from 'react';
import './Login.css';
import { setUserSession } from '../../Utils/Auth';
import { useHistory } from "react-router-dom";
import AuthService from './../../services/AuthService';
function Login() {
    const history = useHistory();
    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const processLogin = (event) => {
        setError(null);
        setLoading(true);
        event.defaultPrevented = true; // to stop refresh!!
        AuthService.login(name,id).then(response => {
            setLoading(false);
            const loggedData = response.data;
            if (loggedData) {
                setUserSession(loggedData.token.token, loggedData.token.name);
                history.push('/dashboard');
            }
        }).catch(error => {
            setLoading(false);
            if (error.response.status === 401) setError(error.response.data.msg);
            else setError("Something went wrong. Please try again later.");
        });


    }
    return (
        <div className="login">
            <div className="login__container">
                <h3>Login</h3>
                <form>
                    <input type="text" className="form__input" value={id} onChange={event => setId(event.target.value)} name="Id" placeholder="Id" />
                    <input type="text" className="form__input" value={name} onChange={event => setName(event.target.value)} name="name" placeholder="Name" />
                    {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}
                    <input type="button" className="login__loginBtn" value={loading ? 'Loading...' : 'Login'} onClick={processLogin} disabled={loading} />
                </form>
            </div>
        </div>
    )
}

export default Login
