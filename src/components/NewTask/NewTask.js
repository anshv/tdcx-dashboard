import React, { useState , useContext, useEffect} from 'react';
import './NewTask.css';
import DashboardService from './../../services/DashboardService';
import { useHistory } from "react-router-dom";
import { TaskContext } from './../../context/TaskContext';
import { Redirect } from 'react-router-dom';

export default function NewTask({taskModalClose}) {
    const [state, dispatch] = useContext(TaskContext);

    const [taskName, setTaskName] = useState("");
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [updateFlag, setUpdateFlag] = useState(false);
    const history = useHistory();
    const [redirect, setRedirect] = useState(false);

    useEffect(() => {
        if (state.task?._id) {
            setTaskName(state.task?.name);
            setUpdateFlag(true);
        } 
    }, []);

    const createTask = async data => {
        DashboardService.addTask(data).then(response => {
            dispatch({
                type: 'CREATE_TASK',
                payload: response.data.task,
              });
            setLoading(false);
            setRedirect(true);
        }).catch(error => {
            setLoading(false);
            if (error.response.status === 401) setError(error.response.data.msg);
            else setError("Something went wrong. Please try again later.");
        });
    };
    const updateTask = async data => {
        DashboardService.updateTask(data, state.task?._id).then(response => {
            dispatch({
                type: 'UPDATE_TASK',
                payload: response.data.task,
              });
            setLoading(false);
            setRedirect(true);
        }).catch(error => {
            setLoading(false);
            if (error.response.status === 401) setError(error.response.data.msg);
            else setError("Something went wrong. Please try again later.");
        });
    };
    const processTask = event => {
        setError(null);
        setLoading(true);
        event.defaultPrevented = true; // to stop refresh!!
        var data = {
            name: taskName
        }
        if (updateFlag) {
            updateTask(data);
        }  else {
            createTask(data);
        }
        taskModalClose();
    }
    if (redirect) {
        return <Redirect to="/dashboard" />;
    }
    return (
        <div className="newTask">
            <div className="newTask__container">
                <h3>{updateFlag ? 'Update Task' : '+ New Task'}</h3>
                <form>
                    <input type="text" className="form__input" value={taskName} onChange={event => setTaskName(event.target.value)} name="Id" placeholder="Task Name" />
                    {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}
                    <input type="button" className="newTask__loginBtn" value={loading ? 'Loading...' : (updateFlag) ? 'Update Task' : '+ New Task'} onClick={processTask} disabled={loading} />
                </form>
            </div>
        </div>
    );
}
