import React, { useState , useContext} from 'react';
import './TaskDetail.css';
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from '@material-ui/icons/Search';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import TaskList from '../TasksList/TasksList';
import NewTask from './../NewTask/NewTask';
import Dialog from '@material-ui/core/Dialog';
import { TaskContext } from './../../context/TaskContext';
function TaskDetail({items, searchTaskEvent}) {
    const [openTaskModal, setTaskModal] = useState(false);
    const [state, dispatch] = useContext(TaskContext);

    const setTaskModalOpen = () => {
      setTaskModal(true);
      dispatch({
        type: 'SELECTED_TASK',
        payload: {},
    });
    }
    const setTaskModalClose = () => {
      setTaskModal(false);
    }
    const searchEvent = (searhText) => {
        searchTaskEvent(searhText);
    }
    return (
        <div className="tasksDetail">
            <div className="taskDetail__top">
                <div className="tasksDetail__left">
                    <h3>Tasks</h3>
                </div>
                <div className="tasksDetail__right">
                    <OutlinedInput
                            id="input-with-icon-adornment"
                            placeholder="Search by task name"
                            InputProps={{ inputProps: { style: { textAlign: 'center', padding: 10, borderRadius: 0 }, }, style: { borderRadius: 0 }, }} variant="outlined"
                            startAdornment={
                            <InputAdornment position="start">
                                <SearchIcon />
                            </InputAdornment>
                            }
                            onKeyUp={(e) => searchEvent(e.target.value)} 
                        />
                        <input type="button" className="tasksDetail__task" value="+New Task" onClick={setTaskModalOpen} />
                        <Dialog open={openTaskModal} onClose={setTaskModalClose}  aria-labelledby="form-dialog-title">
                            <NewTask taskModalClose={() => setTaskModalClose()}/>
                        </Dialog>
                </div>
            </div>
            <div className="tasksDetail__container">
                    {items?.map((item, index) => {
                        return (
                            <TaskList
                                item={item}
                                index={index}
                            />
                        );
                    })}
            </div>
        </div>
        
    )
}

export default TaskDetail
