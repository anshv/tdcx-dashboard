import React, {useState, useContext} from 'react'
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import './TasksList.css';
import NewTask from './../NewTask/NewTask';
import { TaskContext } from './../../context/TaskContext';
import DashboardService from './../../services/DashboardService';
import { Redirect } from 'react-router-dom';

import Dialog from '@material-ui/core/Dialog';

function TasksList({item,index}) {
    const [openTaskModal, setTaskModal] = useState(false);
    const [state, dispatch] = useContext(TaskContext);
    const [redirect, setRedirect] = useState(false);
    const [error, setError] = useState(null);
    const [checkboxCheck,setCheckboxCheck] = useState(false);
    const setTaskModalOpen = () => {
        setTaskModal(true);
    }
    const setTaskModalClose = () => {
        setTaskModal(false);
    }
    const handleEdit = (id) => {
        setTaskModalOpen();
        dispatch({
            type: 'SELECTED_TASK',
            payload: item,
        });
    }
    const handleDelete = (id) => {
        DashboardService.deleteTask(id).then(response => {
            dispatch({
                type: 'DELETE_TASK',
                payload: response.data.task,
            });
            setRedirect(true);
        }).catch(error => {
            if (error.response.status === 401) setError(error.response.data.msg);
            else setError("Something went wrong. Please try again later.");
        });
    }
    const handleComplete = (eventValue, id) => {
        var data = {
            completed: eventValue
        }
        DashboardService.updateTask(data, id).then(response => {
            dispatch({
                type: 'UPDATE_TASK',
                payload: response.data.task,
            });
            setCheckboxCheck(eventValue);
        });
    }
    if (redirect) {
        return <Redirect to="/dashboard" />;
    }
    return (
        <div className="tasksList">
            <div className="tasksList__left">
                    <input type="checkbox" className="tasksList__checkbox" onChange={event => handleComplete(event.target.checked,item._id)}/>
                    <span className={"tasksList__name " + (checkboxCheck || item.completed ? 'tasksList__checked' : '')}>{item.name}</span>
            </div>
            <div className="tasksList__right">
                    <EditIcon className="tasksList__editIcon" onClick={() => handleEdit(item._id)} />
                    <DeleteIcon className="tasksList__deleteIcon" onClick={() => handleDelete(item._id)} />
                    <Dialog open={openTaskModal} onClose={setTaskModalClose}  aria-labelledby="form-dialog-title">
                            <NewTask taskModalClose={() => setTaskModalClose()}/>
                    </Dialog>
            </div>
        </div>
    )
}

export default TasksList
