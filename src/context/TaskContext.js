import React, { useReducer, createContext } from 'react';
//create data layer data layer

export const TaskContext = createContext();

const initialState = {
  tasks: {},
  task: {}, // selected or new
  tasksCompleted: 0,
  totalTasks:0,
  message: {}, // { type: 'success|fail', title:'Info|Error'}
};

function reducer(state, action) {
  switch (action.type) {
    case 'FETCH_TASKS': {
      return {
        ...state,
        tasks: action.payload.latestTasks,
        tasksCompleted: action.payload.tasksCompleted,
        totalTasks:action.payload.totalTasks
      };
    }
    case 'SELECTED_TASK': {
        return {
          ...state,
          task: action.payload,
          message: {
          },
        };
    }
    case 'CREATE_TASK': {
        return {
          ...state,
          tasks: [...state.tasks, action.payload],
          message: {
          },
        };
    }
    case 'UPDATE_TASK': {
        const task = action.payload;
        return {
          ...state,
          tasks: state.tasks.map(item =>
            item._id === task._id ? task : item,
          ),
          message: {
          },
        };
    }
    case 'DELETE_TASK': {
        const { _id } = action.payload;
        return {
          ...state,
          tasks: state.tasks.filter(item => item._id !== _id),
          message: {
          },
        };
    }
    case 'SEARCH_TASK': {
        return {
          ...state,
          tasks: state.tasks.filter(item => item.name.indexOf(action.payload) > -1 ),
          message: {
          },
        };
    }
    default:
      throw new Error();
  }
}

export const TaskContextProvider = props => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { children } = props;

  return (
    <TaskContext.Provider value={[state, dispatch]}>
      {children}
    </TaskContext.Provider>
  );
};