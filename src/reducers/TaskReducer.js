export const initialState = {}
const taskReducer = (state, action) => {
    console.log(action)
    const { payload, type } = action;
    switch(type) {
        case "GET_TASKLIST":
            return {
                ...state,
                list: payload,
            };
        case "GET_TASK":
            return {
                ...state,
                tasks: payload,
            };
        default:
            return state;
    }
}

export default taskReducer;