import {getToken} from './../Utils/Auth';
function AuthHeader() {
        return { Authorization: 'Bearer ' + getToken() };
}

export default AuthHeader
