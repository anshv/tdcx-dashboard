import axios from 'axios';
import { BASE_URL } from './../Utils/Constant';

const login = (name, id) => {
   return axios.post(BASE_URL+'/login', { name: name, apiKey: id });
}
export default {login};