import axios from "axios";
import AuthHeader from "./AuthHeader";
import { BASE_URL } from './../Utils/Constant';

const getDashboardData = () => {
    return axios.get(BASE_URL + "/dashboard", { headers: AuthHeader() });
}

const addTask = (data) => {
    return axios.post(BASE_URL + '/tasks',data, { headers: AuthHeader() })
}
const updateTask = (data, id) => {
    return axios.put(BASE_URL + '/tasks/'+id,data, { headers: AuthHeader() })
}
const deleteTask = (id) => {
    return axios.delete(BASE_URL + '/tasks/'+id, { headers: AuthHeader() })
}

export default {getDashboardData, addTask, updateTask, deleteTask};